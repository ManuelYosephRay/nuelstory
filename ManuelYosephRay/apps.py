from django.apps import AppConfig


class ManuelyosephrayConfig(AppConfig):
    name = 'ManuelYosephRay'
